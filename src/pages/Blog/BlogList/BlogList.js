import React, { useState } from "react"
import { Link } from "react-router-dom"
import {
  Card,
  Col,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
} from "reactstrap"

import classnames from "classnames"

//import images
import small from "../../../assets/images/small/bc.jpeg"
import small2 from "../../../assets/images/small/img-6.jpg"

const BlogList = () => {
  const [activeTab, toggleTab] = useState("1")

  const toggle = tab => {
    if (activeTab !== tab) toggleTab(tab)
  }
  return (
    <React.Fragment>
      <Col xl={9} lg={8}>
        <Card className="pt-4 mt-4">
          <div>
            <Row className="justify-content-center">
              <Col xl={8}>
                <div>
                  <Row className="align-items-center">
                    <Col xs={4}>
                      <div>
                        <h5 className="mb-0">News</h5>
                      </div>
                    </Col>

                    <Col xs={8}>
                      <div className="float-end">
                        <ul className="nav nav-pills">
                          <NavItem>
                            <NavLink
                              className="disabled"
                              to="#"
                              tabIndex="-1"
                            >
                              View :
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <Link
                              className="nav-link active"
                              to="blog-list"
                            >
                              <i className="mdi mdi-format-list-bulleted"></i>
                            </Link>
                          </NavItem>
                          <NavItem>
                            <Link to="blog-grid" className="nav-link">
                              <i className="mdi mdi-view-grid-outline"></i>
                            </Link>
                          </NavItem>
                        </ul>
                      </div>
                    </Col>
                  </Row>

                  <hr className="mb-4" />

                  <div>
                    <h5>
                      <Link to="blog-details" className="text-dark">
                  Mengenal Bitcoin
                      </Link>
                    </h5>
                    <p className="text-muted">10 Apr, 2023</p>

                    <div className="position-relative mb-3">
                      <img src={small} alt="" className="img-thumbnail" />
                    </div>

                    <ul className="list-inline">
                      <li className="list-inline-item mr-3">
                        <Link to="#" className="text-muted">
                          <i className="bx bx-purchase-tag-alt align-middle text-muted me-1"></i>{" "}
                          Project
                        </Link>
                      </li>
                      <li className="list-inline-item mr-3">
                        <Link to="#" className="text-muted">
                          <i className="bx bx-comment-dots align-middle text-muted me-1"></i>{" "}
                          12 Comments
                        </Link>
                      </li>
                    </ul>
                    <p>
Mengenal Bitcoin

Sudah lama nama bitcoin terdengar dan orang-orang dengan antusiasnya bertransaksi atau berinvestasi pada salah satu jenis cryptocurrency ini. Keuntungannya pun begitu menggiurkan karena peningkatan valuasinya dari waktu ke waktu begitu luar biasa. Di Indonesia sendiri sudah banyak basis penggunanya, dan beberapa perusahaan berdiri khusus untuk menjadi platform jual beli (transaksi) bitcoin. Bitcoin kerap dianggap sebagai sebuah instrumen investasi baru yang begitu potensial dalam memaksimalkan keuntungan. Sampai pada tahap ini, kamu mungkin sudah tertarik untuk mengenal lebih lanjut apa itu bitcoin, bagaimana cara kerja bitcoin.

Bitcoin adalah sebuah mata uang baru atau uang elektronik yang diciptakan tahun 2009 lalu oleh seseorang yang menggunakan nama samaran Satoshi Nakamoto. Bitcoin utamanya digunakan dalam transaksi di internet tanpa menggunakan perantara alias tidak menggunakan jasa bank. Sama seperti KoinP2P dari KoinWorks, bitcoin menggunakan sistem peer to peer (P2P). Hanya saja, sistemnya bekerja tanpa penyimpanan atau administrator tunggal di mana Departemen Keuangan Amerika Serikat menyebut bitcoin sebagai sebuah mata uang yang terdesentralisasi. Tidak seperti mata uang lain pada umumnya, bitcoin tidak bergantung pada satu penerbit utama. Bitcoin menggunakan sebuah database yang didistribusikan dan menyebar ke node-node dari sebuah jaringan P2P ke jurnal transaksi. Ia juga menggunakan kriptografi untuk menyediakan berbagai fungsi keamanan dasar, seperti memastikan bahwa bitcoin hanya dapat digunakan oleh orang yang memang memilikinya, dan tidak pernah boleh dilakukan lebih dari satu kali. Alhasil, bitcoin bisa digunakan dalam berbagai jenis transaksi seperti membeli server internet, membeli produk fashion dan lain sebagainya. Banyak juga orang yang hanya memperjualbelikan bitcoin sebagai salah satu kegiatan berinvestasi yang menguntungkan, dan banyak yang menjadi kaya raya karenanya. Hal tersebut dipengaruhi oleh harga bitcoin yang mencapai ratusan juta rupiah pada tahun 2017 silam. Berbeda dengan uang yang biasanya disimpan di rekening bank, bitcoin pada dasarnya disimpan dalam komputer pribadi dengan format file wallet, atau disimpan dalam wallet yang disediakan oleh pihak ketiga. Kepemilikannya pun tidak memerlukan identitas, alias bisa dimiliki oleh seorang anonim.
                    </p>

                    <div>
                      <Link to="#" className="text-primary">
                        Read more <i className="mdi mdi-arrow-right"></i>
                      </Link>
                    </div>
                  </div>

                  <hr className="my-5" />


                  <div className="text-center">
                    <ul className="pagination justify-content-center pagination-rounded">
                      <li className="page-item disabled">
                        <Link to="#" className="page-link">
                          <i className="mdi mdi-chevron-left"></i>
                        </Link>
                      </li>
                      <li className="page-item active">
                        <Link to="#" className="page-link">
                          1
                        </Link>
                      </li>
                      <li className="page-item">
                        <Link to="#" className="page-link">
                          2
                        </Link>
                      </li>
                      <li className="page-item">
                        <Link to="#" className="page-link">
                          3
                        </Link>
                      </li>
                      <li className="page-item">
                        <Link to="#" className="page-link">
                          ...
                        </Link>
                      </li>
                      <li className="page-item">
                        <Link to="#" className="page-link">
                          10
                        </Link>
                      </li>
                      <li className="page-item">
                        <Link to="#" className="page-link">
                          <i className="mdi mdi-chevron-right"></i>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Card>
      </Col>
    </React.Fragment>
  )
}

export default BlogList;