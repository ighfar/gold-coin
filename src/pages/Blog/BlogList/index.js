import React from "react"
import MetaTags from 'react-meta-tags';
import { Container, Row } from "reactstrap"

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

import BlogList from "./BlogList"
import RightBar from "./RightBar"

function index(props) {
  return (
    <React.Fragment>
      <div className="page-content bg-black mt-4">
        <MetaTags className="text-white">
          <title>NurGolds | News</title>
        </MetaTags>
        <div className="container">
        <div className="align-items-center">
          <Row>
            <BlogList />
            <RightBar />
          </Row>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default index
